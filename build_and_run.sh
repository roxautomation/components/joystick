#!/bin/bash

IMG_NAME="registry.gitlab.com/roxautomation/machines/beta/joystick"
WEB_PORT=6001

# build docker image
docker build -t $IMG_NAME .

# run docker image
docker run -d --restart unless-stopped -p $WEB_PORT:80   $IMG_NAME
