"""
 automation of tasks, run with envoke

 Copyright (c) 2023 ROX Automation - Jev Kuznetsov
"""


from invoke import task


DOCKER_USER = "roxauto"
IMG = "joystick"
WEB_PORT = 6001
SOCKET_PORT = 32090


@task
def build(ctx):
    """build docker image"""
    ctx.run(f"docker build -t {DOCKER_USER}/{IMG} .")


@task(pre=[build])
def run(ctx):
    """run docker image"""
    ctx.run(f"docker run --rm -p {WEB_PORT}:80  {DOCKER_USER}/{IMG}")


@task
def buildx(ctx, push: bool = False):
    """build multi-arch docker image"""

    platforms = "linux/amd64,linux/arm64"

    # create buildx builder
    try:
        ctx.run("docker buildx create --name mybuilder --use --bootstrap")
    except:  # pylint: disable=bare-except # noqa
        print("did not create builder, it probably already exists")

    params = ["build", "--platform", platforms, "-t", f"{DOCKER_USER}/{IMG}", "."]

    if push:
        params.append("--push")

    ctx.run("docker buildx " + " ".join(params))
