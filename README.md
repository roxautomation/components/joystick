# rox-joystick
---

Version: 0.1.0


Simple web ui joystick.

## How it works

* a web page with javascript is served (default port 6001)
* data is sent as `{x:int,y:int}` to port `32090`. Both x and y have a range of -100..100


## Demo

1. in terminal 1 run `serve.sh`, webpage can be accessed at [http://localhost:6001](http://localhost:6001)
2. in terminal 2run `receive.sh`, (this runs `reciever.py` example scipt)


## Use pre-built image

run `launch.sh`

## Build and run locally

`build_and_run.sh`
